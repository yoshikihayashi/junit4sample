/**
 *
 */
package jp.co.junit4.sample;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author hayashi.yoshiki
 *
 */
public class ValidatorUtilityTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void checkFizzBuzz_01() {
		int num = 9;
		ValidatorUtility.checkFizzBuzz(num);
		assertEquals("Fizz", ValidatorUtility.checkFizzBuzz(num));
	}

	@Test
	public void checkFizzBuzz_02() {
		int num = 20;
		ValidatorUtility.checkFizzBuzz(num);
		assertEquals("Buzz", ValidatorUtility.checkFizzBuzz(num));
	}

	@Test
	public void checkFizzBuzz_03() {
		int num = 45;
		ValidatorUtility.checkFizzBuzz(num);
		assertEquals("FizzBuzz", ValidatorUtility.checkFizzBuzz(num));
	}

	@Test
	public void checkFizzBuzz_04() {
		int num = 44;
		ValidatorUtility.checkFizzBuzz(num);
		assertEquals("44", ValidatorUtility.checkFizzBuzz(num));
	}

	@Test
	public void checkFizzBuzz_05() {
		int num = 46;
		ValidatorUtility.checkFizzBuzz(num);
		assertEquals("46", ValidatorUtility.checkFizzBuzz(num));
	}


}
