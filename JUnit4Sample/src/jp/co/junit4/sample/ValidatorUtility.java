package jp.co.junit4.sample;

public class ValidatorUtility {
	public static void main(String[] args) {
        // 挙動の確認用に1〜100までの整数を引数にして「checkFizzBuzz」を呼び出す
        for(int i = 1; i <= 100; i++) {
        	System.out.println(checkFizzBuzz(i));
        }
    }

    public static String checkFizzBuzz(int num) {
    	String fizz = "Fizz";
    	String buzz = "Buzz";
    	if(num % 3 == 0 && num % 5 == 0) {
    		return fizz + buzz;
    	}else if(num % 3 == 0) {
    		return fizz;
    	}else if(num % 5 == 0) {
    		return buzz;
    	}
    	return String.valueOf(num);
    }
}
